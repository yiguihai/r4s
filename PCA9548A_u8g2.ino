#include <Wire.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <TCA9548.h>  // https://github.com/RobTillaart/TCA9548 库
#include <Arduino.h>
#include <SPI.h>
#include <U8g2lib.h>
#include <TimeLib.h>


#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET -1
#define buttonPin D3  // 按钮引脚D3

// 初始化 PCA9548A，设置 I2C 地址
PCA9548 MP(0x70);
uint8_t channels = 0;
U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, D1, D2, D0);

const char *ssid = "FriendlyWrt-RTL8811CU";
const char *password = "ygh15177542493";
//const char *serverAddress = "192.168.1.1";  // 服务器地址
const int serverPort = 1234;  // 服务器端口

uint64_t lastReceivedBytes = 0;
uint64_t lastSentBytes = 0;

// 在全局范围内定义时区
const int TIME_ZONE_OFFSET = 8;  // 根据你的时区进行调整

const size_t bufferSize = 1024;  // json缓冲大小根据需要调整

const unsigned char net_close[] PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x80, 0xFF, 0x03, 0x00, 0x00, 0x00, 0xC0, 0xFF, 0x0F, 0x00, 0xC0, 0x01, 0xF0, 0xEF, 0x1F,
  0x00, 0xC0, 0x01, 0xF8, 0x01, 0x3F, 0x00, 0xC0, 0x01, 0x7C, 0x00, 0x3C, 0xC0, 0xC0, 0x01, 0x3E, 0x00, 0x78, 0xE0, 0xC1, 0x01, 0x1F, 0x00, 0x78,
  0xE0, 0xC3, 0x83, 0x0F, 0x00, 0xF0, 0xC0, 0xC7, 0xC1, 0x07, 0x00, 0xF0, 0x80, 0xCF, 0xE1, 0x03, 0x00, 0xF0, 0x00, 0x0F, 0xF0, 0x01, 0x00, 0xF0,
  0x00, 0x0E, 0xF0, 0x00, 0x00, 0xF0, 0x00, 0x00, 0x70, 0x00, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0xF0, 0x0F, 0x70, 0x00, 0x00, 0x78,
  0xF0, 0x0F, 0xFF, 0x03, 0x00, 0x78, 0xF0, 0x8F, 0xFF, 0x0F, 0x00, 0x3C, 0x00, 0xC0, 0xFF, 0x1F, 0x00, 0x1E, 0x00, 0xF0, 0x07, 0x3F, 0x00, 0x1F,
  0x00, 0xF8, 0x01, 0x3C, 0x80, 0x0F, 0x00, 0x7C, 0x00, 0x18, 0xC0, 0x07, 0x00, 0x3C, 0x00, 0x00, 0xE0, 0x03, 0x00, 0x1F, 0x00, 0x00, 0xF0, 0x01,
  0x80, 0x0F, 0x00, 0x00, 0xF8, 0x00, 0xC0, 0x07, 0x00, 0x00, 0x7C, 0x00, 0xE0, 0x03, 0x00, 0x00, 0x3E, 0x00, 0xF0, 0x01, 0x1C, 0x00, 0x1F, 0x00,
  0xF8, 0x00, 0x3C, 0xC0, 0x0F, 0x00, 0x78, 0x00, 0xFC, 0xF0, 0x07, 0x00, 0x3C, 0x00, 0xF8, 0xFF, 0x01, 0x00, 0x1E, 0x00, 0xF0, 0xFF, 0xF8, 0x00,
  0x1E, 0x00, 0xC0, 0x3F, 0xFE, 0x03, 0x0F, 0x00, 0x00, 0x00, 0xFE, 0x03, 0x0F, 0x00, 0x00, 0x00, 0xFC, 0x01, 0x0F, 0x00, 0x00, 0x03, 0x00, 0x00,
  0x0F, 0x00, 0x80, 0x07, 0x00, 0x00, 0x0F, 0x00, 0xC0, 0x03, 0x0E, 0x00, 0x0F, 0x00, 0xE0, 0x23, 0x1E, 0x00, 0x0F, 0x00, 0xF0, 0x71, 0x3E, 0x00,
  0x1E, 0x00, 0xF8, 0x70, 0x7C, 0x00, 0x1E, 0x00, 0x7C, 0x70, 0xF8, 0x00, 0x3C, 0x00, 0x3F, 0x70, 0xF0, 0x00, 0xFC, 0x80, 0x1F, 0x70, 0x60, 0x00,
  0xF8, 0xF7, 0x0F, 0x70, 0x00, 0x00, 0xF0, 0xFF, 0x03, 0x70, 0x00, 0x00, 0xC0, 0xFF, 0x01, 0x70, 0x00, 0x00, 0x00, 0x7F, 0x00, 0x00, 0x00, 0x00, /*"https://blog.csdn.net/qq_53144843/article/details/121908472 和 https://blog.csdn.net/qq_41868901/article/details/104221495 使用c51格式C:\Users\yiguihai\Pictures\Saved Pictures\07adf5c53efb79b6.bmp",0*/
  /* (48 X 48 )*/
};

void setup() {
  Serial.begin(9600);
  Wire.begin();

  connectToWiFi();
  displaySetup();
}

void loop() {
  static bool screenOn = true;  // 屏幕状态，初始为点亮
  static unsigned long lastButtonPress = 0;
  const unsigned long debounceDelay = 500;  // 防抖延迟，单位毫秒
  static unsigned long lastUpdateTime = 0;
  const unsigned long updateInterval = 1000;  // 更新间隔，单位毫秒

  // 读取按钮状态
  int buttonState = digitalRead(buttonPin);

  // 检测按钮是否按下
  if (buttonState == LOW && millis() - lastButtonPress > debounceDelay) {

    Serial.print(screenOn);
    Serial.print(" ");
    Serial.print(lastButtonPress);
    Serial.print(" ");
    Serial.println(debounceDelay);

    // 切换屏幕状态
    screenOn = !screenOn;

    // 记录按钮按下的时间
    lastButtonPress = millis();
  }

  // 根据屏幕状态控制显示
  if (screenOn) {
    // 屏幕点亮状态
    if (millis() - lastUpdateTime >= updateInterval) {
      updateData();
      lastUpdateTime = millis();
    }
  } else {
    // 屏幕关闭状态
    for (int channel = 0; channel < 2; ++channel) {
      MP.selectChannel(channel);
      // 清空显示
      u8g2.clearBuffer();
      // 发送缓冲区内容到显示器
      u8g2.sendBuffer();
    }
  }
}

void connectToWiFi() {
  WiFi.begin(ssid, password);

  unsigned long startTime = millis();

  while (WiFi.status() != WL_CONNECTED && millis() - startTime < 15000) {
    delay(1500);
    Serial.println("Connecting to WiFi...");
  }

  if (WiFi.status() == WL_CONNECTED) {
    Serial.println('\n');               // 换行
    Serial.print("Connected to ");      // 输出连接成功的提示
    Serial.println(WiFi.SSID());        // 输出连接的WiFI名称
    Serial.print("IP address:\t");      // 输出IP地址提示
    Serial.println(WiFi.localIP());     // 输出NodeMCU的IP地址
    Serial.print("RSSI:\t");            // 输出WiFi信号强度提示
    Serial.println(WiFi.RSSI());        // 输出WiFi信号强度
    Serial.print("MAC Address:\t");     // 输出MAC地址提示
    Serial.println(WiFi.macAddress());  // 输出WiFi模块的MAC地址
  } else {
    Serial.println("Failed to connect to WiFi. Please check your credentials or network.");
    Serial.println(WiFi.status());
    // 可以选择进一步处理连接失败的情况
  }
}

void displaySetup() {
  if (MP.begin() == false) {
    Serial.println("Multiplexer error");
    for (;;)
      ;
  }
  channels = MP.channelCount();
  Serial.println(channels);

  for (int channel = 0; channel < 2; ++channel) {
    MP.selectChannel(channel);
    Serial.println(channel);

    u8g2.begin();  // 初始化 U8g2
    //u8g2.enableUTF8Print();  // enable UTF8 support for the Arduino print() function
    //u8g2.setFont(u8g2_font_unifont_t_chinese2);  // use chinese2 for all the glyphs of "你好世界"
    u8g2.clearBuffer();
    u8g2.sendBuffer();
    delay(500);
  }
}


void updateData() {
  // Get the current gateway IP address
  IPAddress gatewayIP = WiFi.gatewayIP();

  // Use the new API
  WiFiClient wifiClient;
  HTTPClient http;
  String url = "http://" + gatewayIP.toString() + ":" + String(serverPort) + "/?json";

  // Use the new API
  http.begin(wifiClient, url);

  int httpCode = http.GET();

  if (httpCode == HTTP_CODE_OK) {
    // Read the response and parse JSON
    String payload = http.getString();
    MP.selectChannel(0);  // 在第一个通道上显示系统信息
    displaySystemInfo(payload);
    MP.selectChannel(1);
    displayTimeAndDate(payload);  // 在第二个通道上显示时间和日期
  } else {
    Serial.println("HTTP request failed: " + String(httpCode));
    for (int channel = 0; channel < 2; ++channel) {
      MP.selectChannel(channel);
      displayDisconnected();  // 无法连接到服务端的出错显示
    }
  }

  http.end();
}

void displaySystemInfo(String jsonData) {
  DynamicJsonDocument jsonBuffer(bufferSize);
  deserializeJson(jsonBuffer, jsonData);

  drawSquare();

  // 设置字体
  u8g2.setFont(u8g2_font_6x10_tf);  // 选择字体，你可以根据需要更改
  u8g2.setFontPosTop();

  // 显示数据
  int lineHeight = u8g2.getAscent() - u8g2.getDescent();  // 计算行高

  //u8g2.drawStr(4, 0 * lineHeight, ("Temp: " + String(jsonBuffer["cpu_temperature"]) + "℃").c_str());
  u8g2.drawStr(4, 0 * lineHeight, ("Temp: " + String(jsonBuffer["cpu_temperature"]) + " C").c_str());
  u8g2.drawStr(4, 1 * lineHeight, ("Fan: " + String(jsonBuffer["pwmfan"])).c_str());

  if (jsonBuffer["internet_interface_ipv4"] != nullptr && jsonBuffer["internet_interface_ipv6"] != nullptr) {
    String ipv4Interface = jsonBuffer["internet_interface_ipv4"].as<const char *>();
    String ipv6Interface = jsonBuffer["internet_interface_ipv6"].as<const char *>();
    u8g2.drawStr(4, 2 * lineHeight, ("IPv4: " + String(jsonBuffer["ipv4"])).c_str());

    if (ipv4Interface == ipv6Interface) {
      u8g2.drawStr(4, 3 * lineHeight, ("Net: " + ipv4Interface).c_str());
    }
    //u8g2.drawStr(4, 3 * lineHeight, ("IPv6: " + String(jsonBuffer["ipv6"])).c_str());
  } else if (jsonBuffer["internet_interface_ipv4"] != nullptr) {
    u8g2.drawStr(4, 2 * lineHeight, ("IPv4: " + String(jsonBuffer["ipv4"])).c_str());
    u8g2.drawStr(4, 3 * lineHeight, ("Net V4: " + String(jsonBuffer["internet_interface_ipv4"].as<const char *>())).c_str());
  } else if (jsonBuffer["internet_interface_ipv6"] != nullptr) {
    u8g2.drawStr(4, 2 * lineHeight, ("IPv6: " + String(jsonBuffer["ipv6"])).c_str());
    u8g2.drawStr(4, 3 * lineHeight, ("Net V6: " + String(jsonBuffer["internet_interface_ipv6"].as<const char *>())).c_str());
  }

  uint64_t currentReceivedBytes = jsonBuffer["ipv4_stats"]["ReceivedBytes"];
  uint64_t currentSentBytes = jsonBuffer["ipv4_stats"]["SentBytes"];

  int64_t downloadSpeed = currentReceivedBytes - lastReceivedBytes;
  int64_t uploadSpeed = currentSentBytes - lastSentBytes;

  lastReceivedBytes = currentReceivedBytes;
  lastSentBytes = currentSentBytes;

  u8g2.drawStr(4, 4 * lineHeight, ("Down: " + (downloadSpeed > 0 ? formatBytes(downloadSpeed) + "/s" : formatBytes(currentReceivedBytes))).c_str());
  u8g2.drawStr(4, 5 * lineHeight, ("Up: " + (uploadSpeed > 0 ? formatBytes(uploadSpeed) + "/s" : formatBytes(currentSentBytes))).c_str());

  uint64_t uptimeNanos = jsonBuffer["uptime"];
  u8g2.drawStr(2, 6 * lineHeight, ("UpTime: " + formatUptime(uptimeNanos)).c_str());

  // 发送缓冲区到显示屏
  u8g2.sendBuffer();
}

String formatBytes(uint64_t bytes) {
  const char *units[] = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB", "BB" };
  uint64_t power = 1024;
  uint8_t idx = 0;

  while (bytes >= power && idx < sizeof(units) / sizeof(units[0])) {
    bytes /= power;
    idx++;
  }

  char buffer[16];
  sprintf(buffer, "%.2f %s", static_cast<float>(bytes), units[idx]);

  return String(buffer);
}

String formatUptime(uint64_t uptimeNanos) {
  const uint64_t nanoPerSecond = 1000000000ULL;
  const uint64_t nanoPerMinute = 60ULL * nanoPerSecond;
  const uint64_t nanoPerHour = 60ULL * nanoPerMinute;
  const uint64_t nanoPerDay = 24ULL * nanoPerHour;

  uint64_t days = uptimeNanos / nanoPerDay;
  uptimeNanos %= nanoPerDay;

  uint64_t hours = uptimeNanos / nanoPerHour;
  uptimeNanos %= nanoPerHour;

  uint64_t minutes = uptimeNanos / nanoPerMinute;
  uptimeNanos %= nanoPerMinute;

  uint64_t seconds = uptimeNanos / nanoPerSecond;

  return String(days) + "d " + String(hours) + "h " + String(minutes) + "m " + String(seconds) + "s";
}

void displayDisconnected() {
  drawSquare();

  int imageWidth = 48;  // 图片宽度
  int imageHeight = 48; // 图片高度

  int centerX = (SCREEN_WIDTH - imageWidth) / 2;  // 计算图片在水平居中的X坐标
  int imageY = 0;  // 图片在屏幕顶部的Y坐标

  // 绘制图像
  u8g2.drawXBMP(centerX, imageY, imageWidth, imageHeight, net_close);

  // 设置字体和文本位置
  u8g2.setFont(u8g2_font_scrum_tf);
  
  // 计算文本的Y坐标，使其显示在图片下方
  int textY = SCREEN_HEIGHT - u8g2.getFontAscent() - u8g2.getFontDescent();

  // 显示文本
  u8g2.setCursor((SCREEN_WIDTH - u8g2.getStrWidth("Disconnected...")) / 2, textY);
  u8g2.print("Disconnected");

  const char loadingText[] = "...";  // 添加空格以确保 "LOAD" 循环显示

  // 计算字符串长度
  int textLength = strlen(loadingText);

  // 显示从i开始的字符
  for (int i = 0; i < textLength; ++i) {
    u8g2.print(loadingText[i]);
    u8g2.sendBuffer();
    delay(5);  // 调整显示间隔
  }

  // 清空缓冲区
  u8g2.clearBuffer();
}

void drawSquare() {
  // 清空缓冲区
  u8g2.clearBuffer();

  // 设置矩形的位置和大小
  int x = 0;
  int y = 0;
  int width = u8g2.getDisplayWidth();
  int height = u8g2.getDisplayHeight();

  // 画一个矩形
  u8g2.drawFrame(x, y, width, height);

  // 发送缓冲区内容到显示器
  //u8g2.sendBuffer();

  // 延迟一段时间，可以根据需要调整
  //delay(2000);

  // 清空缓冲区
  //u8g2.clearBuffer();
}

void displayTimeAndDate(String jsonData) {
  DynamicJsonDocument jsonBuffer(bufferSize);
  deserializeJson(jsonBuffer, jsonData);

  // 清空缓冲区
  u8g2.clearBuffer();

  // 获取JSON中的时间戳
  time_t systemTime = jsonBuffer["system_time"];

  // 转换时间戳为可读的时间和日期格式
  tmElements_t tm;
  breakTime(systemTime + (TIME_ZONE_OFFSET * 3600), tm);  // 考虑时区

  // 设置文本位置
  //int lineHeight = u8g2.getAscent() - u8g2.getDescent();

  // 计算日期文本的Y轴起始位置，使其在屏幕中垂直居中显示并在最顶部
  int textYDate = 3;

  // 计算时间文本的Y轴起始位置，使其在屏幕中垂直居中显示并在最底部
  int textYTime = SCREEN_HEIGHT - u8g2.getFontAscent() - 28;

  // 显示日期
  u8g2.setFont(u8g2_font_profont22_mn);
  String years = String(tm.Year + 1970);
  String months = String(tm.Month);
  String days = String(tm.Day);

  // 计算日期文本的X轴起始位置，使其在屏幕中水平居中显示
  int textXDate = (SCREEN_WIDTH - u8g2.getStrWidth((years + "-" + months + "-" + days).c_str())) / 2;
  u8g2.drawStr(textXDate, textYDate, (years + "-" + months + "-" + days).c_str());

  // 显示时间
  u8g2.setFont(u8g2_font_maniac_tf);
  //u8g2.setFont(u8g2_font_mystery_quest_36_tn);
  // 格式化小时、分钟和秒为两位数
  String hours = String(tm.Hour);
  String minutes = String(tm.Minute);
  String seconds = String(tm.Second);

  if (hours.length() < 2) hours = "0" + hours;
  if (minutes.length() < 2) minutes = "0" + minutes;
  if (seconds.length() < 2) seconds = "0" + seconds;

  // 计算时间文本的X轴起始位置，使其在屏幕中水平居中显示
  int textXTime = (SCREEN_WIDTH - u8g2.getStrWidth((hours + ":" + minutes).c_str())) / 2;

  //u8g2.drawStr(textXTime, textYTime, (hours + ":" + minutes + ":" + seconds).c_str());
  u8g2.drawStr(textXTime, textYTime, (hours + ":" + minutes).c_str());

  // 发送缓冲区到显示屏
  u8g2.sendBuffer();
}