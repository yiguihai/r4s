#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET -1
#define buttonPin D3  // 按钮引脚D3
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

const char *ssid = "FriendlyWrt-RTL8811CU";
const char *password = "1234567890";
//const char *serverAddress = "192.168.1.1";  // 服务器地址
const int serverPort = 1234;  // 服务器端口

uint64_t lastReceivedBytes = 0;
uint64_t lastSentBytes = 0;

const unsigned char net_close[] PROGMEM = {
  // 'IMG_202401071936_48x48, 48x48px
  0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00, 0x00,
  0xff, 0x1f, 0x00, 0x00, 0x00, 0x03, 0xff, 0xfe, 0x00, 0x00, 0x00, 0x0f, 0xff, 0xfc, 0x00, 0x00,
  0x00, 0x0f, 0xff, 0xf8, 0x00, 0x00, 0x07, 0x1f, 0xff, 0xf8, 0x00, 0x00, 0x0f, 0xbf, 0xff, 0xf8,
  0x00, 0x00, 0x07, 0xff, 0xff, 0xfc, 0x00, 0x00, 0x03, 0xff, 0xff, 0xfc, 0x00, 0x00, 0x01, 0xff,
  0xff, 0xfc, 0x00, 0x00, 0x00, 0xff, 0xff, 0xfc, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xfc, 0x00, 0x00,
  0x00, 0xff, 0xff, 0xfc, 0x00, 0x00, 0x01, 0xff, 0xff, 0xfc, 0x00, 0x00, 0x03, 0xff, 0xff, 0xfc,
  0x00, 0x00, 0x07, 0xe7, 0xff, 0xf8, 0x00, 0x00, 0x0f, 0xc3, 0xff, 0xf8, 0x00, 0x00, 0x1f, 0x81,
  0xff, 0xf0, 0x00, 0x00, 0x1f, 0x00, 0xff, 0xf0, 0x01, 0x00, 0x1e, 0x00, 0xff, 0xe0, 0x03, 0x80,
  0x1c, 0x01, 0xff, 0xc0, 0x03, 0xc0, 0x00, 0x03, 0xff, 0x80, 0x03, 0xe0, 0x00, 0x07, 0xef, 0x80,
  0x01, 0xf0, 0x00, 0x0f, 0xc7, 0xc0, 0x01, 0xf8, 0x00, 0x1f, 0x83, 0xc0, 0x03, 0xfc, 0x00, 0x3f,
  0x01, 0xc0, 0x07, 0xfe, 0x00, 0x3e, 0x00, 0x80, 0x0f, 0xff, 0x00, 0x3c, 0x00, 0x00, 0x0f, 0xff,
  0x80, 0x00, 0x00, 0x00, 0x1f, 0xff, 0xc0, 0x00, 0x00, 0x00, 0x1f, 0xff, 0xe0, 0x00, 0x00, 0x00,
  0x3f, 0xff, 0xf0, 0x00, 0x00, 0x00, 0x3f, 0xff, 0xf8, 0x00, 0x00, 0x00, 0x3f, 0xff, 0xfc, 0x00,
  0x00, 0x00, 0x3f, 0xff, 0xfe, 0x00, 0x00, 0x00, 0x3f, 0xff, 0xff, 0x00, 0x00, 0x00, 0x3f, 0xff,
  0xff, 0x80, 0x00, 0x00, 0x3f, 0xff, 0xff, 0xc0, 0x00, 0x00, 0x3f, 0xff, 0xff, 0xe0, 0x00, 0x00,
  0x1f, 0xff, 0xfd, 0xf0, 0x00, 0x00, 0x1f, 0xff, 0xf8, 0xe0, 0x00, 0x00, 0x1f, 0xff, 0xf0, 0x00,
  0x00, 0x00, 0x3f, 0xff, 0xf0, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xc0, 0x00, 0x00, 0x00, 0xf8, 0xff,
  0x00, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00
};

void setup() {
  Serial.begin(9600);
  connectToWiFi();
  displaySetup();
}

void loop() {
  static bool screenOn = true;  // 屏幕状态，初始为点亮
  static unsigned long lastButtonPress = 0;
  const unsigned long debounceDelay = 500;  // 防抖延迟，单位毫秒
  static unsigned long lastUpdateTime = 0;
  const unsigned long updateInterval = 1000;  // 更新间隔，单位毫秒

  // 读取按钮状态
  int buttonState = digitalRead(buttonPin);

  // 检测按钮是否按下
  if (buttonState == LOW && millis() - lastButtonPress > debounceDelay) {
    // 切换屏幕状态
    screenOn = !screenOn;

    // 记录按钮按下的时间
    lastButtonPress = millis();
  }

  // 根据屏幕状态控制显示
  if (screenOn) {
    // 屏幕点亮状态
    if (millis() - lastUpdateTime >= updateInterval) {
      updateData();
      lastUpdateTime = millis();
    }
  } else {
    // 屏幕关闭状态
    display.clearDisplay();
    display.display();
  }
}

void connectToWiFi() {
  WiFi.begin(ssid, password);

  unsigned long startTime = millis();

  while (WiFi.status() != WL_CONNECTED && millis() - startTime < 12000) {
    delay(1500);
    Serial.println("Connecting to WiFi...");
  }

  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Failed to connect to WiFi. Please check your credentials or network.");
    // 可以选择进一步处理连接失败的情况
  }
  /*
  Serial.println('\n');               // 换行
  Serial.print("Connected to ");      // 输出连接成功的提示
  Serial.println(WiFi.SSID());        // 输出连接的WiFI名称
  Serial.print("IP address:\t");      // 输出IP地址提示
  Serial.println(WiFi.localIP());     // 输出NodeMCU的IP地址
  Serial.print("RSSI:\t");            // 输出WiFi信号强度提示
  Serial.println(WiFi.RSSI());        // 输出WiFi信号强度
  Serial.print("MAC Address:\t");     // 输出MAC地址提示
  Serial.println(WiFi.macAddress());  // 输出WiFi模块的MAC地址
  */
}

void displaySetup() {
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;
  }

  display.setTextSize(1);  // Set text size to 1, i.e., normal size
  display.display();
  delay(2000);
  display.setTextColor(WHITE);
  display.clearDisplay();

  // 显示连接信息
  display.println("Connected to: " + WiFi.SSID() + "\nIP address: " + WiFi.localIP().toString() + "\nRSSI: " + String(WiFi.RSSI()) + "\nMAC Address: " + WiFi.macAddress());
  display.display();
  delay(1000);
  display.clearDisplay();
}

void updateData() {
  // Get the current gateway IP address
  IPAddress gatewayIP = WiFi.gatewayIP();

  // Use the new API
  WiFiClient wifiClient;
  HTTPClient http;
  String url = "http://" + gatewayIP.toString() + ":" + String(serverPort) + "/?json";

  // Use the new API
  http.begin(wifiClient, url);

  int httpCode = http.GET();

  if (httpCode == HTTP_CODE_OK) {
    // Read the response and parse JSON
    String payload = http.getString();
    parseAndDisplayData(payload);
  } else {
    Serial.println("HTTP request failed: " + String(httpCode));
    displayDisconnected();
  }

  http.end();
}

void parseAndDisplayData(String jsonData) {
  const size_t bufferSize = 1024;  // Adjust as needed
  DynamicJsonDocument jsonBuffer(bufferSize);
  deserializeJson(jsonBuffer, jsonData);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);

  // Display data without a title
  display.setCursor(0, 0);
  display.println("Temp: " + String(jsonBuffer["cpu_temperature"]) + " C");
  display.println("Fan: " + String(jsonBuffer["pwmfan"]));

  if (jsonBuffer["internet_interface_ipv4"] != nullptr && jsonBuffer["internet_interface_ipv6"] != nullptr) {
    String ipv4Interface = jsonBuffer["internet_interface_ipv4"].as<const char *>();
    String ipv6Interface = jsonBuffer["internet_interface_ipv6"].as<const char *>();
    display.println("IPv4: " + String(jsonBuffer["ipv4"]));

    if (ipv4Interface == ipv6Interface) {
      display.println("Net: " + ipv4Interface);
    } else {
      display.println("Net V4: " + ipv4Interface + " V6: " + ipv6Interface);
    }
  } else if (jsonBuffer["internet_interface_ipv4"] != nullptr) {
    display.println("IPv4: " + String(jsonBuffer["ipv4"]));
    display.println("Net V4: " + String(jsonBuffer["internet_interface_ipv4"].as<const char *>()));
  } else if (jsonBuffer["internet_interface_ipv6"] != nullptr) {
    display.println("IPv6: " + String(jsonBuffer["ipv6"]));
    display.println("Net V6: " + String(jsonBuffer["internet_interface_ipv6"].as<const char *>()));
  }

  uint64_t currentReceivedBytes = jsonBuffer["ipv4_stats"]["ReceivedBytes"];
  uint64_t currentSentBytes = jsonBuffer["ipv4_stats"]["SentBytes"];

  int64_t downloadSpeed = currentReceivedBytes - lastReceivedBytes;
  int64_t uploadSpeed = currentSentBytes - lastSentBytes;

  lastReceivedBytes = currentReceivedBytes;
  lastSentBytes = currentSentBytes;

  display.print("Down: ");
  if (downloadSpeed > 0) {
    display.println(formatBytes(downloadSpeed) + "/s");
  } else {
    display.println(formatBytes(currentReceivedBytes));
  }

  display.print("Up: ");
  if (uploadSpeed > 0) {
    display.println(formatBytes(uploadSpeed) + "/s");
  } else {
    display.println(formatBytes(currentSentBytes));
  }

  uint64_t uptimeNanos = jsonBuffer["uptime"];
  display.println("UpTime: " + formatUptime(uptimeNanos));

  display.display();
}

String formatBytes(uint64_t bytes) {
  const char *units[] = { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB", "BB" };
  uint64_t power = 1024;
  uint8_t idx = 0;

  while (bytes >= power && idx < sizeof(units) / sizeof(units[0])) {
    bytes /= power;
    idx++;
  }

  char buffer[16];
  sprintf(buffer, "%.2f %s", static_cast<float>(bytes), units[idx]);

  return String(buffer);
}

String formatUptime(uint64_t uptimeNanos) {
  const uint64_t nanoPerSecond = 1000000000ULL;
  const uint64_t nanoPerMinute = 60ULL * nanoPerSecond;
  const uint64_t nanoPerHour = 60ULL * nanoPerMinute;
  const uint64_t nanoPerDay = 24ULL * nanoPerHour;

  uint64_t days = uptimeNanos / nanoPerDay;
  uptimeNanos %= nanoPerDay;

  uint64_t hours = uptimeNanos / nanoPerHour;
  uptimeNanos %= nanoPerHour;

  uint64_t minutes = uptimeNanos / nanoPerMinute;
  uptimeNanos %= nanoPerMinute;

  uint64_t seconds = uptimeNanos / nanoPerSecond;

  return String(days) + "d " + String(hours) + "h " + String(minutes) + "m " + String(seconds) + "s";
}

void displayDisconnected() {
  display.clearDisplay();

  int centerX = (SCREEN_WIDTH - 48) / 2;
  int textX = centerX - 20;  // 调整文本的起始位置

  display.drawBitmap(centerX, 0, net_close, 48, 48, WHITE);

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(textX, 48 + 3);

  const char loadingText[] = "Disconnected...   ";  // 添加空格以确保 "LOAD" 循环显示

  for (int j = 0; j < 15; ++j) {  // 控制每个字符的显示时间
    display.print(loadingText[j]);
    display.display();
    delay(100);  // 调整显示间隔
  }

  display.clearDisplay();
}
