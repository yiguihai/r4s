package main

import (
	"bufio"
	"embed"
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/sevlyar/go-daemon"
)

const (
	pwmFanPath         = "/sys/devices/platform/pwm-fan"
	uptimeFile         = "/proc/uptime"
	cpuTemperaturePath = "/sys/class/thermal/thermal_zone0/temp"
	pwmFanStatePath    = "/sys/devices/virtual/thermal/cooling_device0/cur_state"
)

//go:embed web/*
var content embed.FS

type ApiResponse struct {
	CPUTemperature            string        `json:"cpu_temperature"`
	CPUTemperatureDescription string        `json:"cpu_temperature_description"`
	PWMFan                    string        `json:"pwmfan"`
	IPv4                      string        `json:"ipv4"`
	InternetInterfaceIPv4     string        `json:"internet_interface_ipv4"`
	IPv6                      string        `json:"ipv6"`
	InternetInterfaceIPv6     string        `json:"internet_interface_ipv6"`
	Uptime                    time.Duration `json:"uptime"`
	UptimeString              string        `json:"uptime_string"`
	KernelVersion             string        `json:"kernel_version"`
	IPv4Stats                 NetworkStats  `json:"ipv4_stats"`
	IPv6Stats                 NetworkStats  `json:"ipv6_stats"`
	SystemTime                int64         `json:"system_time"`
	FormattedTime             string        `json:"formatted_time"`
}

type NetworkStats struct {
	ReceivedBytes   string
	ReceivedPackets string
	ReceivedErrors  string
	ReceivedDropped string
	ReceivedOverrun string
	ReceivedFrame   string
	SentBytes       string
	SentPackets     string
	SentErrors      string
	SentDropped     string
	SentOverrun     string
	SentCollisions  string
}

func main() {
	runInBackground := flag.Bool("d", false, "Run in background")
	port := flag.Int("p", 1234, "Port to listen on")
	showHelp := flag.Bool("h", false, "Show help")
	flag.Parse()

	if *showHelp {
		printHelp()
		return
	}

	if *runInBackground {
		daemonize(*port)
		return
	}

	startServer(*port)
}

func daemonize(port int) {
	cntxt := &daemon.Context{
		//PidFileName: "daemon.pid",
		//PidFilePerm: 0644,
		//LogFileName: "daemon.log",
		//LogFilePerm: 0640,
		WorkDir: "./",
		Umask:   027,
		Args:    os.Args,
	}

	d, err := cntxt.Reborn()
	if err != nil {
		log.Fatal("Failed to daemonize:", err)
	}
	if d != nil {
		return
	}
	defer cntxt.Release()

	log.Println("Server is running in the background on port", port)
	startServer(port)
}

func startServer(port int) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		isJSONRequest := strings.Contains(r.URL.String(), "?json")

		if isJSONRequest {
			handleJSONRequest(w)
		} else {
			handleHTMLRequest(w)
		}
	})

	server := &http.Server{
		Addr:    fmt.Sprintf("[::]:%d", port),
		Handler: nil,
	}

	log.Printf("Server is listening on [::]:%d\n", port)

	if err := server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

func getUptime() (time.Duration, error) {
	content, err := ioutil.ReadFile(uptimeFile)
	if err != nil {
		return 0, fmt.Errorf("failed to read %s: %v", uptimeFile, err)
	}

	fields := strings.Fields(string(content))
	if len(fields) < 1 {
		return 0, fmt.Errorf("unexpected content in %s", uptimeFile)
	}

	uptimeSeconds, err := strconv.ParseFloat(fields[0], 64)
	if err != nil {
		return 0, fmt.Errorf("failed to parse uptime: %v", err)
	}

	return time.Duration(int64(uptimeSeconds)) * time.Second, nil
}

func formatUptime(uptime time.Duration) string {
	roundedSeconds := int64(uptime.Seconds() + 0.5)
	roundedUptime := time.Duration(roundedSeconds) * time.Second

	days := int(roundedUptime / (time.Hour * 24))
	hours := int((roundedUptime % (time.Hour * 24)) / time.Hour)
	minutes := int((roundedUptime % time.Hour) / time.Minute)
	seconds := int(roundedSeconds % 60)

	return fmt.Sprintf("%d 天 %02d 小时 %02d 分钟 %02d 秒", days, hours, minutes, seconds)
}

func getCPUtemperature() (string, error) {
	content, err := ioutil.ReadFile(cpuTemperaturePath)
	if err != nil {
		return "", err
	}

	return parseTemperature(content), nil
}

func getPWMFanStatus() (string, error) {
	coolingDeviceState, err := ioutil.ReadFile(pwmFanStatePath)
	if err != nil {
		return "", fmt.Errorf("Failed to read fan status: %v", err)
	}

	state, err := strconv.Atoi(strings.TrimSpace(string(coolingDeviceState)))
	if err != nil {
		return "", fmt.Errorf("Failed to parse fan status: %v", err)
	}

	switch state {
	case 0:
		return "off", nil
	case 1, 2, 3, 4:
		return fmt.Sprintf("speed-%d", state), nil
	default:
		return "unknown", nil
	}
}

func parseTemperature(content []byte) string {
	temperatureStr := strings.TrimSpace(string(content))
	temperature, err := strconv.ParseFloat(temperatureStr, 64)
	if err != nil {
		return fmt.Sprintf("Failed to parse temperature: %v", err)
	}
	return fmt.Sprintf("%.2f", temperature/1000.0)
}

func getConfig() (ipv4, internet4, ipv6, internet6 string, err error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return "", "", "", "", fmt.Errorf("Failed to get network interfaces: %v", err)
	}

	for _, iface := range interfaces {
		addrs, err := iface.Addrs()
		if err != nil {
			return "", "", "", "", fmt.Errorf("Failed to get addresses for interface %s: %v", iface.Name, err)
		}

		for _, addr := range addrs {
			ip, _, err := net.ParseCIDR(addr.String())
			if err != nil {
				return "", "", "", "", fmt.Errorf("Failed to parse address %s: %v", addr.String(), err)
			}

			if ip.To4() != nil {
				ipv4 = ip.String()
				internet4 = iface.Name
			} else {
				ipv6 = ip.String()
				internet6 = iface.Name
			}
		}
	}

	return ipv4, internet4, ipv6, internet6, nil
}

func getKernelVersion() (string, error) {
	content, err := ioutil.ReadFile("/proc/version")
	if err != nil {
		return "", fmt.Errorf("Failed to read /proc/version: %v", err)
	}

	return string(content), nil
}

func getNetworkStats(interfaceName string) (NetworkStats, error) {
	filePath := "/proc/net/dev"

	file, err := os.Open(filePath)
	if err != nil {
		return NetworkStats{}, fmt.Errorf("Failed to open file: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		if strings.Contains(line, interfaceName+":") {
			return parseNetworkStats(line)
		}
	}

	if err := scanner.Err(); err != nil {
		return NetworkStats{}, fmt.Errorf("Error reading file: %v", err)
	}

	return NetworkStats{}, fmt.Errorf("Interface %s not found", interfaceName)
}

func parseNetworkStats(line string) (NetworkStats, error) {
	fields := strings.Fields(line)
	if len(fields) >= 17 {
		return NetworkStats{
			ReceivedBytes:   fields[1],
			ReceivedPackets: fields[2],
			ReceivedErrors:  fields[3],
			ReceivedDropped: fields[4],
			ReceivedOverrun: fields[5],
			ReceivedFrame:   fields[6],
			SentBytes:       fields[9],
			SentPackets:     fields[10],
			SentErrors:      fields[11],
			SentDropped:     fields[12],
			SentOverrun:     fields[13],
			SentCollisions:  fields[14],
		}, nil
	}

	return NetworkStats{}, fmt.Errorf("Unexpected format: %s", line)
}

func handleHTMLRequest(w http.ResponseWriter) {
	htmlContent, err := content.ReadFile("web/index.html")
	if err != nil {
		handleError(w, false, "Failed to read HTML template", http.StatusInternalServerError)
		return
	}

	tmpl, err := template.New("html").Parse(string(htmlContent))
	if err != nil {
		handleError(w, false, "Failed to parse HTML template", http.StatusInternalServerError)
		return
	}

	response := ApiResponse{}
	if err := tmpl.Execute(w, response); err != nil {
		handleError(w, false, "Failed to render HTML", http.StatusInternalServerError)
		return
	}
}

func handleJSONRequest(w http.ResponseWriter) {
	response := ApiResponse{}

	type result struct {
		key   string
		value interface{}
		err   error
	}

	results := make(chan result)
	defer close(results)

	go func() {
		cpuTemp, err := getCPUtemperature()
		results <- result{"cpu_temperature", cpuTemp, err}
	}()

	go func() {
		pwmFanStatus, err := getPWMFanStatus()
		results <- result{"pwmfan_status", pwmFanStatus, err}
	}()

	go func() {
		ipv4, internet4, ipv6, internet6, err := getConfig()
		results <- result{"config", []string{ipv4, internet4, ipv6, internet6}, err}
	}()

	go func() {
		uptime, err := getUptime()
		results <- result{"uptime", uptime, err}
	}()

	go func() {
		kernelVersion, err := getKernelVersion()
		results <- result{"kernel_version", kernelVersion, err}
	}()

	var ipv4, internet4, ipv6, internet6 string

	for i := 0; i < 5; i++ {
		r := <-results
		if r.err != nil {
			handleError(w, true, r.err.Error(), http.StatusInternalServerError)
			return
		}

		switch r.key {
		case "cpu_temperature":
			response.CPUTemperature = r.value.(string)
			response.CPUTemperatureDescription = fmt.Sprintf("%s°C", r.value.(string))
		case "pwmfan_status":
			response.PWMFan = r.value.(string)
		case "config":
			config := r.value.([]string)
			ipv4, internet4, ipv6, internet6 = config[0], config[1], config[2], config[3]
			response.IPv4 = ipv4
			response.InternetInterfaceIPv4 = internet4
			response.IPv6 = ipv6
			response.InternetInterfaceIPv6 = internet6
		case "uptime":
			response.Uptime = r.value.(time.Duration)
			response.UptimeString = formatUptime(r.value.(time.Duration))
		case "kernel_version":
			response.KernelVersion = r.value.(string)
		}
	}

	ipv4Stats, err := getNetworkStats(internet4)
	if err != nil {
		handleError(w, true, fmt.Sprintf("Failed to get network stats for IPv4: %v", err), http.StatusInternalServerError)
		return
	}

	ipv6Stats, err := getNetworkStats(internet6)
	if err != nil {
		handleError(w, true, fmt.Sprintf("Failed to get network stats for IPv6: %v", err), http.StatusInternalServerError)
		return
	}

	currentTime := time.Now()
	// 创建一个固定时区的实例，偏移量为8小时
	fixedZone := time.FixedZone("Asia/Shanghai", 8*60*60)
	systemTime := currentTime.Unix()
	formattedTime := currentTime.In(fixedZone).Format("2006-01-02 15:04:05")

	response.IPv4Stats = ipv4Stats
	response.IPv6Stats = ipv6Stats
	response.SystemTime = systemTime
	response.FormattedTime = formattedTime

	w.Header().Set("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	if err := encoder.Encode(response); err != nil {
		handleError(w, true, "Failed to encode JSON response", http.StatusInternalServerError)
	}
}

func handleError(w http.ResponseWriter, isJSONRequest bool, message string, status int) {
	if isJSONRequest {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, fmt.Sprintf(`{"error": "%s"}`, message), status)
	} else {
		http.Error(w, message, status)
	}
}

func printHelp() {
	fmt.Println("Usage:")
	fmt.Println("  -d\tRun in background")
	fmt.Println("  -p\tPort to listen on")
	fmt.Println("  -h\tShow help")
}
