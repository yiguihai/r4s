#include <DHT11.h>
#include <OneButton.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ST7789.h>
#include <SPI.h>
#include <EEPROM.h>
#include "esp_system.h"

/*
scl - hsp clk
sda - hsp mosi
http://arduino.esp8266.com/stable/package_esp8266com_index.json
https://espressif.github.io/arduino-esp32/package_esp32_index.json
MOSI: 23
MISO: 19
SCK: 18
SS: 5
*/

// ======================== 引脚定义 =========================
#define HALL_PIN 26  // 霍尔传感器引脚
#define DHT_PIN 27   // 温湿度传感器引脚
#define GEAR_PIN 14  // 挡位控制引脚

// 显示屏引脚
#define TFT_CS 5
#define TFT_DC 15
#define TFT_RST 19
#define TFT_BACKLIGHT 25

// 控制按钮引脚
#define BRIGHTNESS_INCREASE_PIN 16
#define BRIGHTNESS_DECREASE_PIN 17
#define CLEAR_SCREEN_PIN 21

// EEPROM地址定义
#define EEPROM_BRIGHTNESS_ADDR 0

#define DEBOUNCE_DELAY 35  // 消抖延迟时间（单位：毫秒）

// ===================== 传感器与显示屏实例 ===================
DHT11 dht11(DHT_PIN);
Adafruit_ST7789 display = Adafruit_ST7789(TFT_CS, TFT_DC, TFT_RST);

// ======================== 按钮实例 =========================
OneButton button(GEAR_PIN, true);
OneButton buttonIncrease(BRIGHTNESS_INCREASE_PIN, true);
OneButton buttonDecrease(BRIGHTNESS_DECREASE_PIN, true);
OneButton clearScreenButton(CLEAR_SCREEN_PIN, true);

// ====================== 全局变量与常量 ======================

volatile int lastHallSensorState = LOW;
volatile unsigned long lastDebounceTime = 0;  // 上次有效脉冲的时间
volatile unsigned long hallSensorPulseCount = 0;
unsigned long lastSpeedUpdateTime = 0;

int speed = 0;
float totalDistance = 0.0;
int rpm = 0;

const float wheelCircumference = 1.27;  // 轮子周长
int brightness = 255;                     // 亮度
bool screenOn = true;                     // 屏幕状态
const int screenRotation = 2;             // 屏幕旋转
const int pwmRange = 255;
const int brightnessStepSize = 26;
const int brightnessChangeDelay = 130;    // 调整亮度的延迟时间（毫秒）
bool adjustingBrightness = false;         // 亮度调节标志
unsigned long lastAdjustmentTime = 0;     // 最后一次调整的时间
const int adjustmentDisplayDelay = 2000;  // 无操作后恢复显示的延迟时间

enum Gear {
  GEAR_LOW,
  GEAR_MID,
  GEAR_HIGH
};

Gear currentGear = GEAR_LOW;
unsigned long startTime;

unsigned long lastSensorReadTime = 0;
const unsigned long sensorReadInterval = 2000;

int lastTemperature = 0;
int lastHumidity = 0;

bool errorDisplayed = false;  // 定义一个全局变量来标记错误是否已显示

// ========================= 图像数据 =========================
// extern const unsigned char landscape_planets_stars[];
// extern const unsigned char the_letter[];
extern const unsigned char temperature[];
extern const unsigned char humidity[];
extern const unsigned char distance[];
extern const unsigned char gear[];
extern const unsigned char motor[];
extern const unsigned char timer[];

// ========================= 功能函数 =========================

// 更新速度和距离
void updateSpeedAndDistance() {
  unsigned long currentTime = millis();
  if (currentTime - lastSpeedUpdateTime >= 1000) {
    speed = (hallSensorPulseCount * wheelCircumference) / 1000 * 3600;
    totalDistance += (hallSensorPulseCount * wheelCircumference) / 1000;
    rpm = hallSensorPulseCount;
    hallSensorPulseCount = 0;
    lastSpeedUpdateTime = currentTime;
  }
}

// 读取并更新温湿度数据
void readAndUpdateSensorData() {
  unsigned long currentTime = millis();
  if (currentTime - lastSensorReadTime >= sensorReadInterval) {
    int temperature, humidity;
    int result = dht11.readTemperatureHumidity(temperature, humidity);
    if (result == 0) {
      lastTemperature = temperature;
      lastHumidity = humidity;
    } else {
      Serial.println(DHT11::getErrorString(result));
      displayNotification(DHT11::getErrorString(result).c_str());  // 转换 String 到 const char*
      return;
    }
    lastSensorReadTime = currentTime;
  }
}

// 显示错误信息
void displayNotification(const char* message) {
  static int16_t lastErrorX = 0, lastErrorY = 120;          // 保存上一次文本显示的位置
  static uint16_t lastErrorWidth = 0, lastErrorHeight = 0;  // 保存上一次文本的宽高

  // 获取新文本的边界
  int16_t x1, y1;
  uint16_t w, h;
  display.setTextSize(1);
  display.getTextBounds(message, lastErrorX, lastErrorY, &x1, &y1, &w, &h);

  // 清除上次绘制区域
  if (w < lastErrorWidth) {  // 清除宽度部分
    display.fillRect(lastErrorX + w, lastErrorY, lastErrorWidth - w, h, ST77XX_WHITE);
  }

  if (h < lastErrorHeight) {  // 清除高度部分
    display.fillRect(lastErrorX, lastErrorY + h, w, lastErrorHeight - h, ST77XX_WHITE);
  }

  // 设置文本属性并绘制新的错误信息
  display.setCursor(lastErrorX, lastErrorY);
  display.setTextColor(ST77XX_BLACK, ST77XX_WHITE);
  display.println(message);

  // 保存当前文本的边界信息
  lastErrorWidth = w;
  lastErrorHeight = h;
  errorDisplayed = true;
}

// 清屏并重绘初始图像
void clearAndRedrawScreen() {
  Serial.println("清屏修复显示异常！");
  display.fillScreen(ST77XX_WHITE);
  errorDisplayed = false;  // 重置错误显示的标志
  drawInitialGraphics();
}

// 绘制初始图像
void drawInitialGraphics() {
  int yLine = 128;
  display.drawLine(0, yLine, display.width() - 1, yLine, ST77XX_BLACK);
  int xLine = display.width() / 2;
  display.drawLine(xLine, yLine, xLine, display.height() - 1, ST77XX_BLACK);
  int spacing = 64;
  int screenWidth = display.width();
  for (int y = yLine + spacing; y < display.height(); y += spacing) {
    display.drawLine(0, y, screenWidth / 2, y, ST77XX_BLACK);
    display.drawLine(screenWidth / 2, y, screenWidth, y, ST77XX_BLACK);
  }
  display.drawLine(0, 119, display.width(), 119, ST77XX_BLACK);
  display.drawLine(0, display.height() - 1, display.width(), display.height() - 1, ST77XX_BLACK);
  displayImage();
}

// 更新显示内容
void updateDisplay() {
  updateSpeedDisplay();
  updateTemperatureHumidityDisplay();
  updateRPMDisplay();
  updateDistanceDisplay();
  updateGearDisplay();
  updateElapsedTimeDisplay();
}

// 更新速度显示
void updateSpeedDisplay() {
  int displaySpeed = speed < 100 ? speed : 99;
  char speedStr[3];
  sprintf(speedStr, "%02d", displaySpeed);

  int16_t x1, y1;
  uint16_t textWidth, textHeight;
  display.setTextSize(12);
  display.getTextBounds(speedStr, 0, 0, &x1, &y1, &textWidth, &textHeight);
  int x = ((display.width() - textWidth) / 2) + 6;
  int y = (128 - textHeight) / 2;

  display.setTextColor(ST77XX_BLACK, ST77XX_WHITE);
  display.setCursor(x, y);
  display.printf("%s", speedStr);
}

// 更新温度和湿度显示
void updateTemperatureHumidityDisplay() {
  display.setTextSize(2);
  display.cp437(true);

  // 温度显示相关变量
  static int16_t lastTempX = 52, lastTempY = 152;
  static uint16_t lastTempWidth = 0, lastTempHeight = 0;

  // 湿度显示相关变量
  static int16_t lastHumX = 180, lastHumY = 152;
  static uint16_t lastHumWidth = 0, lastHumHeight = 0;

  // 温度字符串
  char temperatureStr[20];
  sprintf(temperatureStr, "%d %cC", lastTemperature, 0xF8);  // 0xF8 为摄氏度符号

  // 湿度字符串
  char humidityStr[10];
  sprintf(humidityStr, "%d %%", lastHumidity);

  // 获取温度字符串的边界
  int16_t tempX1, tempY1;
  uint16_t tempW, tempH;
  display.getTextBounds(temperatureStr, lastTempX, lastTempY, &tempX1, &tempY1, &tempW, &tempH);

  // 清除温度区域
  if (lastTempWidth > tempW) {  // 清除宽度部分
    display.fillRect(lastTempX + tempW, lastTempY, lastTempWidth - tempW, tempH, ST77XX_WHITE);
  }
  if (lastTempHeight > tempH) {  // 清除高度部分
    display.fillRect(lastTempX, lastTempY + tempH, tempW, lastTempHeight - tempH, ST77XX_WHITE);
  }

  // 绘制新的温度文本
  display.setCursor(tempX1, tempY1);
  display.setTextColor(ST77XX_BLACK, ST77XX_WHITE);
  display.print(temperatureStr);

  // 更新温度边界信息
  lastTempX = tempX1;
  lastTempY = tempY1;
  lastTempWidth = tempW;
  lastTempHeight = tempH;

  // 获取湿度字符串的边界
  int16_t humX1, humY1;
  uint16_t humW, humH;
  display.getTextBounds(humidityStr, lastHumX, lastHumY, &humX1, &humY1, &humW, &humH);

  // 清除湿度区域
  if (lastHumWidth > humW) {  // 清除宽度部分
    display.fillRect(lastHumX + humW, lastHumY, lastHumWidth - humW, humH, ST77XX_WHITE);
  }
  if (lastHumHeight > humH) {  // 清除高度部分
    display.fillRect(lastHumX, lastHumY + humH, humW, lastHumHeight - humH, ST77XX_WHITE);
  }

  // 绘制新的湿度文本
  display.setCursor(humX1, humY1);
  display.print(humidityStr);

  // 更新湿度边界信息
  lastHumX = humX1;
  lastHumY = humY1;
  lastHumWidth = humW;
  lastHumHeight = humH;
}

// 更新 RPM 显示
void updateRPMDisplay() {
  static int16_t lastX = 66, lastY = 216;
  static uint16_t lastWidth = 0, lastHeight = 0;

  char rpmStr[10];  // 假设 RPM 不会超过99999
  sprintf(rpmStr, "%d", rpm);

  int16_t x1, y1;
  uint16_t w, h;
  display.getTextBounds(rpmStr, lastX, lastY, &x1, &y1, &w, &h);

  // 清除上次绘制区域
  if (lastWidth > w) {  // 清除宽度部分
    display.fillRect(lastX + w, lastY, lastWidth - w, h, ST77XX_WHITE);
  }

  if (lastHeight > h) {  // 清除高度部分
    display.fillRect(lastX, lastY + h, w, lastHeight - h, ST77XX_WHITE);
  }

  // 绘制新的文本
  display.setCursor(x1, y1);
  display.print(rpmStr);

  // 保存当前文本的边界信息
  lastX = x1;
  lastY = y1;
  lastWidth = w;
  lastHeight = h;
}

// 更新距离显示
void updateDistanceDisplay() {
  static int16_t lastX = 182, lastY = 220;
  static uint16_t lastWidth = 0, lastHeight = 0;

  char distanceStr[20];
  if (totalDistance < 1.0) {
    sprintf(distanceStr, "%.1f M", totalDistance * 1000);
  } else {
    sprintf(distanceStr, "%.1f KM", totalDistance);
  }

  int16_t x1, y1;
  uint16_t w, h;
  display.setTextSize(1);
  display.getTextBounds(distanceStr, lastX, lastY, &x1, &y1, &w, &h);

  // 清除上次绘制区域
  if (w < lastWidth) {  // 清除宽度部分
    display.fillRect(lastX + w, lastY, lastWidth - w, h, ST77XX_WHITE);
  }

  if (h < lastHeight) {  // 清除高度部分
    display.fillRect(lastX, lastY + h, w, lastHeight - h, ST77XX_WHITE);
  }

  // 绘制新的文本
  display.setCursor(x1, y1);
  display.setTextColor(ST77XX_BLACK, ST77XX_WHITE);
  display.println(distanceStr);

  // 保存当前文本的边界信息
  lastX = x1;
  lastY = y1;
  lastWidth = w;
  lastHeight = h;
}

// 更新挡位显示
void updateGearDisplay() {
  static int16_t lastGearX = 66, lastGearY = 280;         // 保存上一次文本显示的位置
  static uint16_t lastGearWidth = 0, lastGearHeight = 0;  // 保存上一次文本的宽高

  // 根据当前挡位选择文本
  const char* gearStr = "";
  switch (currentGear) {
    case GEAR_LOW:
      gearStr = "LOW";
      display.setTextColor(ST77XX_YELLOW, ST77XX_WHITE);
      break;
    case GEAR_MID:
      gearStr = "MID";
      display.setTextColor(ST77XX_BLUE, ST77XX_WHITE);
      break;
    case GEAR_HIGH:
      gearStr = "HIGH";
      display.setTextColor(ST77XX_CYAN, ST77XX_WHITE);
      break;
  }

  // 获取新文本的边界
  int16_t x1, y1;
  uint16_t w, h;
  display.setTextSize(2);
  display.getTextBounds(gearStr, lastGearX, lastGearY, &x1, &y1, &w, &h);

  // 清除上次绘制区域
  if (w < lastGearWidth) {  // 清除宽度部分
    display.fillRect(lastGearX + w, lastGearY, lastGearWidth - w, h, ST77XX_WHITE);
  }

  if (h < lastGearHeight) {  // 清除高度部分
    display.fillRect(lastGearX, lastGearY + h, w, lastGearHeight - h, ST77XX_WHITE);
  }

  // 绘制新的文本
  display.setCursor(lastGearX, lastGearY);
  display.print(gearStr);

  // 保存当前文本的边界信息
  lastGearWidth = w;
  lastGearHeight = h;
}

// 更新经过时间显示
void updateElapsedTimeDisplay() {
  unsigned long elapsedTime = (millis() - startTime) / 1000;
  int hours = elapsedTime / 3600;
  elapsedTime %= 3600;
  int minutes = elapsedTime / 60;
  int seconds = elapsedTime % 60;

  display.setTextSize(1);
  display.setCursor(176, 287);
  display.setTextColor(ST77XX_BLACK, ST77XX_WHITE);
  display.printf("%02d:%02d:%02d", hours, minutes, seconds);
}

// 更新循环时间显示
void updateLoopDurationDisplay(unsigned long loopDuration) {

  static int16_t lastX = 5, lastY = 120;
  static uint16_t lastWidth = 0, lastHeight = 0;

  char loopDurationStr[20];
  sprintf(loopDurationStr, "Update: %lu ms", loopDuration);

  int16_t x1, y1;
  uint16_t w, h;
  display.setTextSize(1);
  display.getTextBounds(loopDurationStr, lastX, lastY, &x1, &y1, &w, &h);

  // 如果当前文本宽度或高度比上次小，清除上次绘制区域
  if (w < lastWidth) {  // 清除宽度部分
    display.fillRect(lastX + w, lastY, lastWidth - w, h, ST77XX_WHITE);
  }

  if (h < lastHeight) {  // 清除高度部分
    display.fillRect(lastX, lastY + h, w, lastHeight - h, ST77XX_WHITE);
  }

  // 绘制新的文本
  display.setCursor(x1, y1);
  display.setTextColor(ST77XX_BLACK, ST77XX_WHITE);
  display.print(loopDurationStr);

  // 保存当前文本的边界信息
  lastX = x1;
  lastY = y1;
  lastWidth = w;
  lastHeight = h;
}

void changeBrightness(int adjustment) {
  brightness += adjustment;
  if (brightness > pwmRange) brightness = pwmRange;
  if (brightness < 0) brightness = 0;
  analogWrite(TFT_BACKLIGHT, brightness);
  String notificationMessage = "Brightness: " + String(brightness);
  displayNotification(notificationMessage.c_str());
  Serial.println(notificationMessage);
}

// 更新亮度，传入增量值
void adjustBrightness(int delta) {
  static unsigned long lastChangeTime = 0;
  if (millis() - lastChangeTime >= brightnessChangeDelay) {
    brightness += delta;  // 根据增量值调整亮度

    // 限制亮度范围
    if (brightness > pwmRange) {
      brightness = pwmRange;
    } else if (brightness < 0) {
      brightness = 0;
    }

    analogWrite(TFT_BACKLIGHT, brightness);
    displayBrightnessCircle();  // 绘制亮度百分比圆
    adjustingBrightness = true;
    lastAdjustmentTime = millis();

    lastChangeTime = millis();
  }
}

// 绘制亮度的圆形
void drawBrightnessCircle() {
  int centerX = display.width() / 2;
  int centerY = display.height() / 2;
  int radius = display.width() / 4;  // 半径为屏幕宽度的四分之一

  // 绘制背景颜色（建议填充矩形以便背景清晰可见）
  display.fillRect(0, 0, display.width(), display.height(), ST77XX_WHITE);  // 白色背景

  // 绘制填充圆形
  display.fillCircle(centerX, centerY, radius, ST77XX_BLACK);  // 用黑色填充圆形
}

int previousTextWidth = 0;   // 上次绘制文本的宽度
int previousTextHeight = 0;  // 上次绘制文本的高度
int previousTextX = 0;       // 上次绘制文本的左上角X坐标
int previousTextY = 0;       // 上次绘制文本的左上角Y坐标

void displayBrightnessPercentage() {
  int centerX = display.width() / 2;
  int centerY = display.height() / 2;

  // 计算并显示百分比
  int brightnessPercent = (brightness * 100) / pwmRange;
  char percentStr[5];
  sprintf(percentStr, "%d%%", brightnessPercent);

  display.setTextSize(3);
  display.setTextColor(ST77XX_WHITE, ST77XX_BLACK);  // 字体颜色为白色，背景为黑色

  // 获取当前文本的边界
  int16_t x1, y1;
  uint16_t w, h;
  display.getTextBounds(percentStr, 0, 0, &x1, &y1, &w, &h);

  // 如果当前文本宽度或高度比上次小，清除上次绘制区域
  if (w < previousTextWidth) {  // 如果现在的宽度比上次小，清除字符的右侧部分
    display.fillRect(previousTextX + w, previousTextY, previousTextWidth - w, h, ST77XX_WHITE);
  }

  if (h < previousTextHeight) {  // 如果现在的高度比上次小，清除字符的下侧部分
    display.fillRect(previousTextX, previousTextY + h, w, previousTextHeight - h, ST77XX_WHITE);
  }

  // 设置新文本的游标位置
  display.setCursor(centerX - w / 2, centerY - h / 2);
  display.print(percentStr);

  // 更新 previousTextX, previousTextY, previousTextWidth 和 previousTextHeight 以便下次使用
  previousTextX = centerX - w / 2;
  previousTextY = centerY - h / 2;
  previousTextWidth = w;
  previousTextHeight = h;
}

// 控制显示的主要函数
void displayBrightnessCircle() {
  if (!adjustingBrightness) drawBrightnessCircle();  // 绘制圆形
  displayBrightnessPercentage();                     // 显示百分比
}

// 切换屏幕开关状态
void toggleScreen() {
  if (screenOn) {
    // 关闭屏幕
    display.fillScreen(ST77XX_WHITE);
    digitalWrite(TFT_BACKLIGHT, LOW);  // 关闭背光
    analogWrite(TFT_BACKLIGHT, 0);     // 设置 PWM 输出为 0
  } else {
    // 打开屏幕
    digitalWrite(TFT_BACKLIGHT, HIGH);       // 打开背光
    analogWrite(TFT_BACKLIGHT, brightness);  // 恢复 PWM 输出到之前的亮度
    clearAndRedrawScreen();                  // 恢复屏幕显示
  }

  int storedBrightness = EEPROM.read(EEPROM_BRIGHTNESS_ADDR);
  // 如果亮度改变，则存储新的亮度设置到 EEPROM
  if (brightness != storedBrightness) {
    EEPROM.write(EEPROM_BRIGHTNESS_ADDR, brightness);
    EEPROM.commit();
  }

  screenOn = !screenOn;  // 切换屏幕状态
  delay(600);            // 延时以避免抖动
  Serial.print("屏幕状态变量: ");
  Serial.println(screenOn ? "true" : "false");
}

// 挡位按钮点击处理
void handleGearButtonClick() {
  currentGear = static_cast<Gear>((currentGear + 1) % 3);
}

// 霍尔传感器中断处理函数
void IRAM_ATTR handleHallSensorPulse() {
  int currentHallState = digitalRead(HALL_PIN);  // 读取当前霍尔传感器的电平状态

  // 检查本次电平状态是否与上次不同，且是否已超过消抖延迟时间
  if (currentHallState != lastHallSensorState) {
    unsigned long currentTime = millis();  // 获取当前时间
    if (currentTime - lastDebounceTime > DEBOUNCE_DELAY) {
      hallSensorPulseCount++;                  // 增加脉冲计数
      lastDebounceTime = currentTime;          // 更新上次有效脉冲的时间
      lastHallSensorState = currentHallState;  // 更新上次的电平状态

      // 打印当前状态
      /*
    Serial.print(currentHallState);
    Serial.println(currentHallState ? " -> HIGH 灯灭" : " -> LOW 灯亮");
    */
    }
  }
}

// 显示图像数据
void displayImage() {
  struct ImageData {
    const unsigned char* image;
    int x;
    int y;
  };

  ImageData images[] = {
    { temperature, 0, 136 },
    { humidity, 121, 136 },
    { motor, 0, 200 },
    { distance, 121, 200 },
    { gear, 0, 264 },
    { timer, 121, 264 }
  };

  for (int i = 0; i < sizeof(images) / sizeof(images[0]); ++i) {
    display.drawRGBBitmap(images[i].x, images[i].y, (uint16_t*)images[i].image, 48, 48);
  }
}

// ========================= setup 函数 =========================
void setup() {
  Serial.begin(115200);
  EEPROM.begin(512);
  startTime = millis();

  Serial.print("MOSI: ");
  Serial.println(MOSI);
  Serial.print("MISO: ");
  Serial.println(MISO);
  Serial.print("SCK: ");
  Serial.println(SCK);
  Serial.print("SS: ");
  Serial.println(SS);
  Serial.print("TFT_CS: ");
  Serial.println(TFT_CS);
  Serial.print("TFT_DC: ");
  Serial.println(TFT_DC);
  Serial.print("TFT_RST: ");
  Serial.println(TFT_RST);

  brightness = EEPROM.read(EEPROM_BRIGHTNESS_ADDR);
  if (brightness > pwmRange) brightness = pwmRange;

  pinMode(HALL_PIN, INPUT_PULLUP);
  pinMode(GEAR_PIN, INPUT_PULLUP);
  pinMode(TFT_BACKLIGHT, OUTPUT);
  analogWrite(TFT_BACKLIGHT, brightness);

  // 显示屏初始化
  display.init(240, 320);               // 根据屏幕分辨率调整参数
  display.setRotation(screenRotation);  // 设置屏幕旋转（根据你的需要）

  attachInterrupt(digitalPinToInterrupt(HALL_PIN), handleHallSensorPulse, CHANGE);

  dht11.setDelay(0);

  button.attachClick(handleGearButtonClick);
  buttonIncrease.attachClick([]() {
    changeBrightness(brightnessStepSize);
  });
  buttonDecrease.attachClick([]() {
    changeBrightness(-brightnessStepSize);
  });

  // 长按时逐步增加亮度
  buttonIncrease.attachDuringLongPress([]() {
    adjustBrightness(1);
  });  // 调整亮度增加 1
  // 长按时逐步减少亮度
  buttonDecrease.attachDuringLongPress([]() {
    adjustBrightness(-1);
  });  // 调整亮度减少 1

  // 设置 clearScreenButton 的单击、双击和长按事件
  clearScreenButton.attachClick(clearAndRedrawScreen);  // 单击调用清屏重绘
  clearScreenButton.attachDoubleClick(toggleScreen);    // 双击调用切换开关屏幕
  clearScreenButton.attachLongPressStart([]() {         // 长按调用 esp_restart()
    Serial.println("长按3秒后重启...");
    for (int countdown = 3; countdown > 0; --countdown) {
      displayNotification(("Restarting in " + String(countdown) + "...").c_str());
      delay(1000);
    }
    esp_restart();
  });

  clearAndRedrawScreen();
}

// ========================= loop 函数 =========================
void loop() {
  unsigned long loopStartTime = millis();

  button.tick();
  buttonIncrease.tick();
  buttonDecrease.tick();
  clearScreenButton.tick();

  // 只有在屏幕打开的情况下才更新速度和距离和读取温湿度
  if (screenOn) {
    //hallSensorPulseCount = random(1, 13);
    updateSpeedAndDistance();
    readAndUpdateSensorData();

    if (!adjustingBrightness || (millis() - lastAdjustmentTime > adjustmentDisplayDelay)) {
      if (adjustingBrightness) {
        clearAndRedrawScreen();  // 恢复屏幕显示
        adjustingBrightness = false;
      } else {
        updateDisplay();  // 仅在屏幕开启时更新显示内容
      }
    }

    unsigned long loopEndTime = millis();
    unsigned long loopDuration = loopEndTime - loopStartTime;
    if (!errorDisplayed && !adjustingBrightness) {
      updateLoopDurationDisplay(loopDuration);
    }
    //delay(1000);
  }
}
